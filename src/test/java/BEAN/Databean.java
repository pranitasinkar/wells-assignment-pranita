package BEAN;

import com.qmetry.qaf.automation.data.BaseDataBean;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class Databean extends BaseDataBean {
	@Randomizer (length=3,type=RandomizerTypes.LETTERS_ONLY)
	private String Challengingsituationsfaced;

	public String getChallengingsituationsfaced() {
		return Challengingsituationsfaced;
	}
	@Randomizer(length=4,type=RandomizerTypes.LETTERS_ONLY)
	private String solutionprovided;

	public String getSolution_provided() {
		return solutionprovided;
	}
@Randomizer(length=5,type=RandomizerTypes.LETTERS_ONLY)
private String benefits_accrued;
@Randomizer(length=5,type=RandomizerTypes.LETTERS_ONLY)
private String rational_for_nomination;

public String getBenefits_accrued() {
	return benefits_accrued;
}
public void setBenefits_accrued(String benefits_accrued) {
	this.benefits_accrued = benefits_accrued;
}
public String getRational_for_nomination() {
	return rational_for_nomination;
}
public void setRational_for_nomination(String rational_for_nomination) {
	this.rational_for_nomination = rational_for_nomination;
}
@Randomizer (length=3,type=RandomizerTypes.LETTERS_ONLY)
private String expenseTitle;

public String getexpenseTitlefordatabean() {
	return expenseTitle;
}
	public String getExpenseProjectTitle() {
	return expenseProjectTitle;
	}
	public void setExpenseProjectTitle(String ExpenseProjectTitle) {
		this.expenseProjectTitle = expenseProjectTitle;
}
	@Randomizer (length=3,type=RandomizerTypes.LETTERS_ONLY)
	private String expenseProjectTitle;
	@Randomizer (length=3,type=RandomizerTypes.LETTERS_ONLY)
	private String expnseCategory;

	public String getContributionKey() {
		return contributionKey;
	}
	public String getExpnseCategory() {
		return expnseCategory;
	}
	@Randomizer (length=3,type=RandomizerTypes.LETTERS_ONLY)
	private String contributionKey;

	
	
		
}
