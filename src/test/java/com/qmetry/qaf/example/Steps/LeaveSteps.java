package com.qmetry.qaf.example.Steps;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.Pages.LeavePage;

public class LeaveSteps {
	LeavePage leave = new LeavePage();

	@QAFTestStep(description = "{0} page should be displayed")
	public void verifyTitlePage(String title) {
		leave.VerifyTitlePage(title);
		// CommonStep.verifyPresent("Leave.title.loc");
	}

	@QAFTestStep(description = "user select {0} menu from menu list")
	public void selectMenu(String menu) {
		leave.selectMenuFromMenuList(menu);
	}

	@QAFTestStep(description = "user select {0} submenu from submenu list")
	public void select_submenu_from_submenuList(String submenu) {
		leave.selectSubMenu(submenu);
	}

	@QAFTestStep(description = "user click on leavetype dropdown")
	public void dropdown() {
		leave.clickOnLeavedropdown();
	}

	@QAFTestStep(description = "user select leave type as {0}")
	public void select_leave_type(String leavetype) {
		leave.selectLeaveType(leavetype);
	}

	@QAFTestStep(description = "{0} should be displayed")
	public void verifyFields(String field) {

		QAFExtendedWebElement leaveTypefromlist = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("newpost.field"), field));
		Validator.verifyThat(leaveTypefromlist.getText(), Matchers.equalToIgnoringCase(field));
	}

	@QAFTestStep(description = "user select leave date")
	public void selectDate() {
		leave.selectDate();
	}

	@QAFTestStep(description = "user select leave reason as {0}")
	public void selectleaveReason(String reason) {
		leave.selectLeaveReason(reason);
	}

	@QAFTestStep(description = "user select leaveday as {0}")
	public void selectLeaveDays(String day) {
		leave.selectLeaveDay(day);
	}

	@QAFTestStep(description = "single day leave should be applied")
	public void single_day_leave_should_be_applied() {
		leave.appliedLeaveStatus();
	}

	@QAFTestStep(description = "list of header menu should be displayed")
	public void headerMenuShouldDisplayed() {
		leave.leaveHeaderMenuList();
	}

	@QAFTestStep(description = "{0} message should be displayed")
	public void visibleLeaveMessage(String message) {
		leave.VisibleAppliedLeaveMessage(message);
	}

	@QAFTestStep(description = "leave balance should be excluding holidays")
	public void checkLeaveBalanceForEmployee() {
		leave.checkLeaveBalance();
	}

	@QAFTestStep(description = "user select back button")
	public void clickONBackButton() {

	}

	@QAFTestStep(description = "user select {0} menu from hamburger")
	public void clickOnMenuForManagerView(String menu) {
		leave.clickOnMenuforManagerView(menu);
	}
}
