package com.qmetry.qaf.example.Steps;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.Pages.CommonUtils;
import com.qmetry.qaf.example.Pages.TravelPage;

public class TravelSteps {

	TravelPage travel = new TravelPage();

	
	@QAFTestStep(description = "user select request row of travel tab")
	public void clickOnRequestRowOfTravelTab() {
		System.out.println();
		Validator.verifyTrue(travel.getRequestrow().isDisplayed(), "heders menu are not displaying", "headers menu displaying");
		travel.getRequestrow().click();
		CommonUtils.pageload();
	}

	@QAFTestStep(description = "user should get {0} on requestpage for {1}")

	public void checkPresencyOfButtons(String buttonslist, String loc) {
		travel.userShoulGetOptionsOnRequestPage(buttonslist, loc);

	}

	@QAFTestStep(description = "user should get {0} field")
	public void userShouldGetPurposeOfTravelField(String header) {
		travel.verifyHeaders(header);
	}

	{

	}
}
