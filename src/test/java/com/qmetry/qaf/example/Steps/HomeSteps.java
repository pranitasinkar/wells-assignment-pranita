package com.qmetry.qaf.example.Steps;

import org.openqa.selenium.JavascriptExecutor;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.Pages.CommonUtils;
import com.qmetry.qaf.example.Pages.HomePage;
import com.qmetry.qaf.example.Pages.TravelPage;

public class HomeSteps {
	HomePage homepage = new HomePage();
	TravelPage travel = new TravelPage();

	@QAFTestStep(description = "user select {0} homepage Tab")
	public void clickOnHomePageRequest(String request) throws InterruptedException {

		homepage.clickOnTab(request);

	}

	@QAFTestStep(description = "user switches the view to manager view")
	public void switchView() throws InterruptedException {
		CommonUtils.pageload();
		homepage.getSwitchViewButton().click();
		CommonUtils.pageload();
	}

	@QAFTestStep(description = "user select logout button")
	public void clickOnLogout() throws InterruptedException {
		homepage.clickOnLogOut();
	}

	@QAFTestStep(description = "user navigates to home page")
	public void navigate_to_home_page() {
		homepage.logout();
	}

	@QAFTestStep(description = "user select {0} menu")
	public void clickOnRandRMenu(String menu) throws InterruptedException {
		// homepage.scrolldown();
		homepage.openMainMenu(menu);
	}

	@QAFTestStep(description = "user select {0} for manager view")
	public void clickOnRandRMenuForManagerView(String menu) throws InterruptedException {
		// homepage.scrolldown();
		homepage.displaymenuformagarView(menu);
	}
	@QAFTestStep(description = "user select {0} manager view")
	public void clickOnleaveMenuForManagerView(String menu) throws InterruptedException {
		// homepage.scrolldown();
		homepage.clickOnLeaveMenu(menu);
	}
	

	@QAFTestStep(description = "user select {0} submenu for manager view")
	public void clickOnRandRSubmenu(String submenu) throws InterruptedException {
		CommonUtils.pageload();
		homepage.submenuOfRandR(submenu);
	}

	@QAFTestStep(description = "user select {0} for delivery manager view")
	public void clickOnRandRMenuForDeliveryManagerView(String menu) throws InterruptedException {
		// homepage.scrolldown();
		homepage.openMainMenuForDeliveryManagerView(menu);
	}

	@QAFTestStep(description = "user select {0} submenu")
	public void clickOnNominateSubmenu(String submenu) throws InterruptedException {

		homepage.naminateMenu(submenu);

	}

	@QAFTestStep(description = "user select {0}")
	public void clickOnBrightSpark(String bright) throws InterruptedException {
		homepage.clickOnBright(bright);

	}

	@QAFTestStep(description = "user select {0} request")
	public void brightSparkRequest() {

	}

	@QAFTestStep(description = "get the size of list")
	public void getSize() {
		homepage.testComponet();
	}

	@QAFTestStep(description = "user select {0} submenu for my view")

	public void selectSubMenu(String submenu) {
		homepage.submenuForMyView(submenu);
	}

}
