package com.qmetry.qaf.example.Steps;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.Pages.CommonUtils;
import com.qmetry.qaf.example.Pages.ReimbursementPage;

public class ReimbursementSteps {
	ReimbursementPage reimbursement = new ReimbursementPage();

	@QAFTestStep(description = "user select {0} button from expense list page")
	public void selectNewExpenseButton(String button) {
		reimbursement.clickOnExpenseButton(button);
	}

	@QAFTestStep(description = "user select {0} button after filling the details")
	public void userSelectButton(String button) {
		CommonUtils.pageload();
		reimbursement.clickOnFormButton(button);
	}

	@QAFTestStep(description = "user passes invalid data for expense list")
	public void passDataForExpenseList() {
		CommonUtils.pageload();
		reimbursement.fillingInformationForExpense();
	}

	@QAFTestStep(description = "wrong filled buttons should displayed in red color")
	public void displayFieldsInRedColour() {
		CommonUtils.pageload();
		reimbursement.displayFieldsInRedColour();
	}

	@QAFTestStep(description = "user should get tooltip")

	public void displayTooltip() {
		String str = reimbursement.getExpenseTooltip().getText();
		// Validator.verifyTrue(str., "Tooltip is not displayed", "Tootip is
		// displayed");
		Validator.verifyThat(str, Matchers.containsString("This is a required field."));
	}

	@QAFTestStep(description = "user should verify {0} is required field")
	public void displayRequiredMessage(String RequiredField) {
		reimbursement.displayTooltip(RequiredField);
	}

	@QAFTestStep(description = "user should verify project is required field")
	public void displayTooltipForProject() {
		CommonStep.verifyPresent("expense.project.tooltip");
	}

	@QAFTestStep(description = "user select {0} button for attachment")
	public void user_select_attachment_button(String button) {
		QAFExtendedWebElement expensebutton = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("expense.browse.button"), button));
		Validator.verifyTrue(expensebutton.isDisplayed(), "button is missing", "button is there");
		expensebutton.click();
	}

	@QAFTestStep(description = "image should be uploaded sussessfully")
	public void userSelectImageWithValidSize() throws AWTException {
		StringSelection ss = new StringSelection("D:\\1 doubt.png");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		Robot robot = new Robot();

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		WaitForPageToLoad();
		CommonStep.verifyPresent("expense.browse.image");
	}

	private void WaitForPageToLoad() {
		// TODO Auto-generated method stub

	}

}