package com.qmetry.qaf.example.Steps;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.Pages.BrightsparkPage;
import com.qmetry.qaf.example.Pages.CommonUtils;

public class BrightsparkSteps {

	BrightsparkPage bright = new BrightsparkPage();

	@QAFTestStep(description = "user select {0} button")
	public void selectGroup(String field) {
		bright.selectFields(field);
	}

	@QAFTestStep(description = "user select {0} option")
	public void selectEmployee(String employee) throws InterruptedException {
		bright.selectEmployee(employee);
	CommonUtils.pageload();
	}

	@QAFTestStep(description = "user passes data into {0} field")
	public void enterText(String str) {
		bright.enterData(str);
	}

	@QAFTestStep(description = "user select the {0} card")
	public void selectCard(String card) {
		bright.selectCard(card);
	}

	@QAFTestStep(description = "user should get {0} request for {1}")
	public void brightSparkverify(String employee, String prize) {
		bright.brightSpark(employee, prize);
	}

	@QAFTestStep(description = "user should select {0} request for {1}")
	public void clickOnrewardForEmp(String employee, String prize) {
		bright.clickOnReward(employee, prize);
	}

	@QAFTestStep(description = "user select {0} for {1} whoes status is {2}")
	public void verifyTheStatus(String prize,String employee, String status) {
		bright.veriyStatus(prize, employee , status);
	}

	@QAFTestStep(description = "user passes data into text fields")
	public void data_Passes_Into_Text_Fields() {
		bright.filllngInformation();
	}

	@QAFTestStep(description = "user select {0} requrst for {1}")

	public void clickAward(String employee, String prize) {
		bright.clickOnAward(employee, prize);
	}

	@QAFTestStep(description = "user passes data into text field for pat on back field")
	public void enterData() {
		bright.enterData();
	}

	@QAFTestStep(description = "{0} fields should be displayed")
	public void verifyFieldsOfNewPost(String header) {
		String[] str = header.split(",");
		for (int i = 0; i < str.length; i++) {

			QAFExtendedWebElement titlepage = new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getString("newpost.field"), str[i]));
			Validator.verifyThat(titlepage.isPresent(), Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "{0} block should be displayed")
	public void verifyBlockofthePage(String block) {
		String[] str = block.split(",");
		for (int i = 0; i < str.length; i++) {

			QAFExtendedWebElement titlepage = new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getString("randrstatus.loc"), str[i]));
			Validator.verifyThat(titlepage.isPresent(), Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "{0} value addition field should be displayed")
	public void verifyBlockofValueAdditionField(String block) {
		String[] str = block.split(",");
		for (int i = 0; i < str.length; i++) {

			QAFExtendedWebElement titlepage = new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getString("valueaddition.loc"), str[i]));
			Validator.verifyThat(titlepage.isPresent(), Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "{0} fields should be displayed as mandotory fields")
	public void displayMandatoryField(String header) {
		String[] str = header.split(",");
		for (int i = 0; i < str.length; i++) {

			QAFExtendedWebElement titlepage = new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getString("newpost.field"), str[i]));
			Validator.verifyThat(titlepage.isPresent(), Matchers.equalTo(true));
		}
	}

}