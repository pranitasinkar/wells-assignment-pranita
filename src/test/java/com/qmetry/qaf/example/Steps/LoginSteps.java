package com.qmetry.qaf.example.Steps;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.example.Pages.LoginPage;

public class LoginSteps {
	LoginPage login = new LoginPage();
	

	@QAFTestStep(description = "user launches nest application")
	public void navigateToLogInPage() throws InterruptedException {
		LoginPage login = new LoginPage();
			login.launchSite();
	}

	@QAFTestStep(description = "user passes {0} and {1}")
	public void enter_Username_And_Password(String username, String password) {
		login.enterCredentials(username, password);
	}

	@QAFTestStep(description = "user select LOGIN button")
	public void user_Click_On_Login_Button() throws InterruptedException {
		login.clickOnLogIn();

	}

	@QAFTestStep(description = "logIn page should be displayed")

	public void logInfun() {
		login.logInPage();

	}

	@QAFTestStep(description = "user logIn to the system as {0} and {1}")

	public void logInAndClick(String username, String password) throws InterruptedException {
		login.loginandClickOnLoginButton(username, password);
	}

	@QAFTestStep(description = "user should get {0} error message")
	public void error_Message_User_Cannnot_Be_Empty(String errormessage) {
		login.verifyErrorMessage(errormessage);

	}

	@QAFTestStep(description = "Then User should be logged in successfully")
	public void User_Should_Be_Logged_In_Successfully() {

	}
}
