package com.qmetry.qaf.example.Steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.example.Pages.CommonUtils;
import com.qmetry.qaf.example.Pages.RandRPage;

public class RandRsteps {
	RandRPage randr = new RandRPage();

	@QAFTestStep(description = "user should get {0} as title page")
	public void verifyTitleOfThePage(String title) {
		Reporter.log(title);
		randr.getVerifyTitle(title);
	}

	@QAFTestStep(description = "user should get breadscrum")
	public void verifyBreadscrumOfTheGrandChild() {
		randr.getBreadscrumForGrandChild();
	}

	@QAFTestStep(description = "there should be columns")
	public void verifyTheColumns() {
		randr.verifyHeaderMenu();
	}

	@QAFTestStep(description = "pagination should be present there")
	public void verifyPaginationOfNominationsPage() {
		randr.verifyPagination();
	}

	@QAFTestStep(description = "user nominate same employee more than 1 time")
	public void user_nominate_same_employee_more_than_1_time() {
		// randr.
	}

	@QAFTestStep(description = "user should get {0} as menu list")
	public void user_should_get_HeaderMenu(String headerMenu) {
		randr.randrPageHeaderMenu(headerMenu);
	}

	@QAFTestStep(description = "two date picker should be displayed")
	public void userSHouldGetTwoDatePicker() {
		CommonStep.waitForPresent("startdatecaledar.loc");
		CommonStep.verifyPresent("startdatecaledar.loc");
	}

	@QAFTestStep(description = "enter Keyword textfield should be displayed")
	public void enter_Keyword_textfield_should_be_displayed() {
		// CommonStep.waitForPresent("myPost.textfiled", 3000);
		CommonStep.verifyPresent("myPost.textfiled");
		// Reporter.log(Text(myPost.textfiled.get);

	}

	@QAFTestStep(description = "dropdown should be displayed")
	public void displayDropdown() {
		CommonStep.verifyPresent("mypost.category.dopdown");
	}

	@QAFTestStep(description = "two buttons for {0} and {1} should be displyed")
	public void dispayButtons(String Search, String Reset) {

		CommonStep.waitForPresent("selectfield.loc");
		CommonStep.verifyPresent("selectfield.loc");
	}

	@QAFTestStep(description = "date should be in dd-mm-yyyy format")
	public void displaydate() {

	}

	@QAFTestStep(description = "hide filter icon should be on right uper side of page")
	public void displayFilterIcon() {
		CommonStep.verifyPresent("mypost.filter.icon");
	}

	// @QAFTestStep(description="Pagination should be present on right lowe end
	// of page")
	// public void
	// }
	@QAFTestStep(description = "{0} title should be displayed for manager view")
	public void verifyTitle(String title) {
		randr.displayTitleForManagerView(title);
	}

	@QAFTestStep(description = "user select {0} employee")
	public void selectEmployee(String emp) {
		CommonStep.click("employeedropdown.randrrequest");
		CommonUtils.pageload();
		CommonStep.sendKeys(emp, "employeeTextField.randrrequest");

	}

}
