package com.qmetry.qaf.example.Pages;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class LoginPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	TravelPage travel = new TravelPage();

	@FindBy(locator = "username.loc")
	private QAFWebElement username;
	@FindBy(locator = "password.loc")
	private QAFWebElement password;
	@FindBy(locator = "logInbtn.loc")
	private QAFWebElement logInbtn;
	@FindBy(locator = "blankUserNameError.loc")
	private QAFWebElement blankUserNAme;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
		driver.get("/");
		driver.manage().window().maximize();
		driver.navigate().refresh();
	}

	public QAFWebElement getBlankUserNAme() {
		return blankUserNAme;
	}

	public QAFWebElement getUsername() {
		return username;
	}

	public QAFWebElement getPassword() {
		return password;
	}

	public QAFWebElement getLogInbtn() {
		return logInbtn;
	}

	public void launchSite() throws InterruptedException {
	
		launchPage(null);
		
	}

	public void userName() {
		getUsername().sendKeys("abc1234	");
	}

	public void enterCredentials(String username, String password) {

		
		CommonUtils.pageload();
		CommonStep.verifyPresent("username.loc");
		CommonStep.sendKeys(username, "username.loc");
		CommonStep.verifyPresent("password.loc");
		CommonStep.sendKeys(password, "password.loc");
		CommonUtils.pageload();
	}

	public void clickOnLogIn() throws InterruptedException {
		CommonUtils.pageload();
		Validator.verifyTrue(getLogInbtn().isDisplayed(), "button is not there", "button is there");
		CommonStep.click("logInbtn.loc");
		CommonUtils.pageload();
		CommonUtils.pageload();
	}

	public void verifyErrorMessage(String errormessage) {

		QAFExtendedWebElement msg = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("blankUserNameError.loc"), errormessage));
		Validator.verifyTrue(msg.isDisplayed(), errormessage + "is not there", errormessage + "field is there");

	}

	public void logInPage() {
		boolean stg = getUsername().isDisplayed();
		Validator.verifyTrue(stg, "Username field is not there", "Username field is there");
	}

	public void loginandClickOnLoginButton(String username, String password) throws InterruptedException {
		
		launchSite();
		waitForPageToLoad();
		Validator.verifyTrue(getUsername().isDisplayed(), username+ "field is not there", username + "field is there");
		CommonStep.sendKeys(username, "username.loc");
		CommonStep.sendKeys(password, "password.loc");

		CommonStep.click("logInbtn.loc");
		CommonUtils.pageload();
		waitForPageToLoad();
	}

}
