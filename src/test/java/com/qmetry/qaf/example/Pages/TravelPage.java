package com.qmetry.qaf.example.Pages;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class TravelPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator locator, Object... args) {
	

	}

	@FindBy(locator = "requestroView.loc")
	public QAFWebElement requestrow;

	@FindBy(locator = "buttons.loc")
	public QAFWebElement buttons;

	@FindBy(locator = "travel.page.purpose")
	public List<QAFWebElement> travelPagefieldList;

	@FindBy(locator = "pageLoad.loc")
	public QAFWebElement pageload;

	public QAFWebElement getPageload() {
		return pageload;
	}

	public QAFWebElement getButtons() {
		return buttons;
	}

	public QAFWebElement getRequestrow() {
		return requestrow;
	}

	public static void pageload() {
		CommonStep.waitForNotVisible("pageLoad.loc", 30000);
	}

	public void userShoulGetOptionsOnRequestPage(String data, String loc) {
		Reporter.log("data received" + data);
			String[] fieldList = data.split(",");
			Reporter.log("String received" + Arrays.toString((fieldList)));
		
		}

	

	public void verifyHeaders(String header) {
		String[] str = header.split(",");
		for (int i = 0; i < str.length; i++) {

			QAFExtendedWebElement titlepage = new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getString("travel.page.purpose"), str[i]));
			Validator.verifyThat(titlepage.isPresent(), Matchers.equalTo(true));
		}
	}
}
