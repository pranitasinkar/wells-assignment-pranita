package com.qmetry.qaf.example.Pages;

import org.openqa.selenium.interactions.Actions;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

import BEAN.Databean;

public class ReimbursementPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	Databean databean = new Databean();

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		

	}

	@FindBy(locator = "expense.project")
	private QAFWebElement expenseProject;

	public QAFWebElement getExpenseProject() {
		return expenseProject;
	}

	@FindBy(locator = "expense.tooltip")
	private QAFWebElement expenseTooltip;

	public QAFWebElement getExpenseTooltip() {
		return expenseTooltip;
	}

	@FindBy(locator = "expene.category")
	private QAFWebElement expenseCategory;

	public QAFWebElement getExpenseCategory() {
		return expenseCategory;
	}

	@FindBy(locator = "expenseData.loc")
	private QAFWebElement expenseTitle;

	public QAFWebElement getExpenseTitle() {
		return expenseTitle;
	}

	@FindBy(locator = "expense_button.loc")
	private QAFWebElement newExpenseButton;

	public QAFWebElement getNewExpenseButton() {
		return newExpenseButton;
	}

	public void clickOnExpenseButton(String button) {
		CommonUtils.pageload();
		QAFExtendedWebElement expensebutton = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("expense_button.loc"), button));
		Validator.verifyTrue(expensebutton.isDisplayed(), button + "is not there", button + "is there");
		expensebutton.click();
		CommonUtils.pageload();

	}

	public void clickOnFormButton(String button) {

		QAFExtendedWebElement expensebutton = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("formsbutton.loc"), button));
		expensebutton.click();
		CommonUtils.pageload();

	}

	public void fillingInformationForExpense() {
		Validator.verifyTrue(getExpenseTitle().isDisplayed(), "text fields are missing", "text fields are present");
		getExpenseTitle().sendKeys(databean.getexpenseTitlefordatabean());
		getExpenseProject().sendKeys(databean.getExpenseProjectTitle());
		getExpenseCategory().sendKeys(databean.getExpnseCategory());
	}

	public void displayFieldsInRedColour() {
		CommonUtils.pageload();
		Actions act = new Actions(driver);
		act.moveToElement(expenseProject).release().perform();
	}

	public void displayTooltip(String RequiredField) {

		QAFExtendedWebElement expensetooltip = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getPropertyValue("expense.tooltip"), RequiredField));
		Actions act = new Actions(driver);
		act.moveToElement(expensetooltip).perform();
		expensetooltip.waitForPresent(3000);
	}
}
