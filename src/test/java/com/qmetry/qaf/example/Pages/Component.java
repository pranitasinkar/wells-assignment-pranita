package com.qmetry.qaf.example.Pages;

import java.util.List;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Component extends QAFWebComponent {
	
	public Component(String locator) {
		super(locator);
	}
	
	@FindBy(locator = "seperate.menuloc")
	private QAFWebElement seperatemenulist;
	
	public QAFWebElement getSeperatemenulist() {
		return seperatemenulist;
	}
	@FindBy(locator = "hamburgermenulist.loc")
	private List<QAFWebElement> hamburgermenulist;

	@FindBy(locator = "hamburger.menuimage.loc")
	private QAFWebElement menuimage;

	@FindBy(locator = "hamburger.menuarrow.loc")
	private QAFWebElement menuarrow;

	public QAFWebElement getMenuimage() {
		return menuimage;
	}

	public QAFWebElement getMenuarrow() {
		return menuarrow;
	}

	@FindBy (locator="nominations.header.menu.loc")
	private List<QAFWebElement> nominationsHeaderMenu;
	
	public List<QAFWebElement> getnominatioonsHeaderMenu()
	{
		return nominationsHeaderMenu;
	}
	public List<QAFWebElement> getHamburgermenulist() {
		return hamburgermenulist;
	}
	

}
