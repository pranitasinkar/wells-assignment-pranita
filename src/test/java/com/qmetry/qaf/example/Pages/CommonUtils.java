package com.qmetry.qaf.example.Pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class CommonUtils
{ 

		 	public static void clickUsingJavaScript(WebElement ele) {
		  JavascriptExecutor executor =
		    (JavascriptExecutor) new WebDriverTestBase().getDriver();
		  executor.executeScript("arguments[0].click();", ele);
		 }
	
	public static QAFExtendedWebDriver getDriver() {
		  return new WebDriverTestBase().getDriver();
		 }

	public static void scrollToAxis(int x, int y) {

        ((JavascriptExecutor) new WebDriverTestBase().getDriver())

                     .executeScript("window.scrollTo(" + x + ", " + y + ");");

 }
	public static QAFWebElement getElement(String locKey, String eleText) {
		QAFWebElement buttonEle = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(locKey), eleText, eleText));
		return buttonEle;
	}
	
	public static String getPropString(String key){
		return ConfigurationManager.getBundle().getProperty(key).toString();
	}
	
	
	public static void pageload()
	{
		CommonStep.waitForNotVisible("pageLoad.loc", 2000000000);
	}
	public void verifyLabel(String label)
	{
		QAFWebElement labelpresent = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("mypost.label")));
				
	}
	
}
