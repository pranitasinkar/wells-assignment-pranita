package com.qmetry.qaf.example.Pages;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;;

public class RandRPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	HomePage homepage = new HomePage();
	LeavePage leavepage = new LeavePage();
	RandRPage randr = new RandRPage();

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

	@FindBy(locator = "grandchildTitle.loc")
	private QAFWebElement grandchildTitle;

	public QAFWebElement getGrandchildTitle() {
		return grandchildTitle;
	}

	@FindBy(locator = "mypost.label")
	private QAFWebElement myPostLabel;

	public QAFWebElement getMyPostLabel() {
		return myPostLabel;
	}

	@FindBy(locator = "RandRrequests.header.column")
	private QAFWebElement randrRequestHeaderColumn;

	public QAFWebElement getRandrRequestHeaderColumn() {
		return randrRequestHeaderColumn;
	}

	@FindBy(locator = "breadscrum.loc")
	private QAFWebElement breadscrum;

	public QAFWebElement getBreadscrum(String bread) {
		return CommonUtils.getElement("breadscrum.loc", bread);
	}

	@FindBy(locator = "headerlist.loc")
	private Component headerlist;

	public Component getHeaderList() {
		return headerlist;
	}

	@FindBy(locator = "nominations.pagination.loc")
	private QAFWebElement nominationPagination;

	public QAFWebElement getNominationPagination() {
		return nominationPagination;
	}

	@FindBy(locator = "myPost.textfiled")
	private QAFWebElement myPostTextField;

	public QAFWebElement getMyPostTextField() {
		return myPostTextField;
	}

	public void getVerifyTitle(String title) {
		QAFTestBase.pause(2000);
		QAFExtendedWebElement sub = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("grandchildTitle.loc"), title));
		Reporter.log(sub.getText().trim() + "*******");
		if (getGrandchildTitle().getText().trim().contains(title)) {
			Reporter.logWithScreenShot("Expected Title is present", MessageTypes.Pass);
		}
	}

	public void getBreadscrumForGrandChild() {
		String breadScrumValue = new String();

		breadScrumValue = CommonUtils.getPropString("first.grandchild.breadscrum.prop").toString() + " / "
				+ CommonUtils.getPropString("second.grandchild.breadscrum.prop").toString();
		Reporter.log(breadScrumValue + "+++++++++++++++++");

		Validator.verifyThat(getBreadscrum(CommonUtils.getPropString("first.grandchild.breadscrum.prop").toString())
				.getText() + " / "
				+ getBreadscrum(CommonUtils.getPropString("second.grandchild.breadscrum.prop").toString()).getText(),
				Matchers.equalToIgnoringCase(breadScrumValue));
	}

	public void verifyHeaderMenu() {
		Reporter.log(getHeaderList().getnominatioonsHeaderMenu().size() + "*****");
		String str = leavepage.getLeavemenulist().get(0).getText();

		String[] expected = { "Nominee(s)", "No. of Reward(s)", "Posted Date", "Manager Status", "HR Status" };

		if (expected.length != leavepage.getLeavemenulist().size()) {
			Reporter.log("fail, wrong number of elements found");
		}

		for (int i = 0; i < expected.length; i++) {
			String optionValue = leavepage.getLeavemenulist().get(i).getAttribute("innerHTML");

			if (optionValue.equals(expected[i])) {
				Reporter.log("passed on: " + optionValue);
			} else {
				Reporter.log("failed on: " + optionValue);
			}
		}
	}

	public void verifyPagination() {
		Reporter.log(getNominationPagination().toString() + "");
		Validator.verifyTrue(getNominationPagination().isDisplayed(), "Pagination is not present",
				"Pagination is present");
	}

	public void nominateEmployee(String menu) throws InterruptedException {
		homepage.openMainMenu(menu);

	}

	public void randrPageHeaderMenu(String randrHeaderMenu) {
		String[] str = randrHeaderMenu.split(",");
		for (int i = 0; i < str.length; i++) {
			QAFExtendedWebElement randrPageHeaderMenu = new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getString("randrheadermenu.loc"), str[i]));
			randrPageHeaderMenu.verifyPresent();
		}
	}

	public void checkPresencyOfDatePickers() {
		leavepage.getStartdatecaledar().verifyPresent();
	}

	public void verifyOneTextField(String value) {
		QAFTestBase.pause(2000);
		QAFExtendedWebElement sub = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("mypost.label"), value));
		Reporter.log(sub.getText().trim() + "*******");
		if (getGrandchildTitle().getText().trim().contains(value)) {
			Reporter.logWithScreenShot("value=title", MessageTypes.Pass);
		}
	}

	public void checkTextField() {
		getMyPostTextField().getText();

	}

	public void clickOnAward(String award) {
		QAFTestBase.pause(2000);
		QAFExtendedWebElement sub = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("RandRrequests.header.column"), award));
		sub.isDisplayed();

		getRandrRequestHeaderColumn().getText();
	}

	public void displayTitleForManagerView(String title) {
		QAFTestBase.pause(2000);
		QAFExtendedWebElement sub = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("title.managerview"), title));
		sub.isDisplayed();
		if (sub.getText().equals(title)) {
			Reporter.logWithScreenShot("Title is verified");
		}
		CommonUtils.pageload();

	}
}
