package com.qmetry.qaf.example.Pages;

import java.util.List;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

import BEAN.Databean;

public class BrightsparkPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	Databean databean = new Databean();

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

	@FindBy(locator = "textfield.patonback")
	private QAFWebElement textFieldForPatOnBack;

	public QAFWebElement getTextFieldForPatOnBack() {
		return textFieldForPatOnBack;
	}

	@FindBy(locator = "textfield.loc")
	private QAFWebElement textField;

	public QAFWebElement getTextfield() {
		return textField;
	}

	@FindBy(locator = "selectfield.loc")
	private QAFWebElement fields;

	@FindBy(locator = "brightspark.searchfield.loc")
	private QAFWebElement searchfield;

	@FindBy(locator = "brightspark.messagecard.loc")
	private QAFWebElement messagecard;

	@FindBy(locator = "brightspark.selectcard.loc")
	private QAFWebElement seletcard;

	@FindBy(locator = "request.nominee.loc")
	private List<QAFWebElement> requestnominee;

	public List<QAFWebElement> getRequestnominee() {
		return requestnominee;
	}

	@FindBy(locator = "request.reward.loc")
	private List<QAFWebElement> rewardnames;

	public List<QAFWebElement> getRewardnames() {
		return rewardnames;
	}

	@FindBy(locator = "table.head")
	private List<QAFWebElement> tableHead;

	public List<QAFWebElement> getTableHead() {
		return tableHead;
	}

	@FindBy(locator = "request.seperatenominee.loc")
	private QAFWebElement seperatenominee;

	public QAFWebElement getSeperatenominee() {
		return seperatenominee;
	}

	public QAFWebElement getSeletcard() {
		return seletcard;
	}

	public QAFWebElement getMessagecard() {
		return messagecard;
	}

	public QAFWebElement getSearchfield() {
		return searchfield;
	}

	@FindBy(locator = "manager.status")
	private List<QAFWebElement> managerStatus;

	public List<QAFWebElement> getManagerStatus() {
		return managerStatus;
	}

	@FindBy(locator = "brightspark.dropdowntext.loc")
	private QAFWebElement textDropdownField;

	public QAFWebElement getTextDropdownField() {
		return textDropdownField;
	}

	public List<QAFWebElement> getEmpNames() {
		return empNames;
	}

	@FindBy(locator = "brightspark.drpdown.loc")
	private List<QAFWebElement> empNames;

	public QAFWebElement getFields() {
		return fields;
	}

	public void selectFields(String field) {
		QAFTestBase.pause(3000);
		QAFExtendedWebElement msg = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("selectfield.loc"), field));
		Validator.verifyTrue(msg.isDisplayed(), field + "is missing", field + "is present");
		msg.click();
		CommonUtils.pageload();
		CommonUtils.pageload();
		CommonUtils.pageload();

	}

	public void selectEmployee(String employee) throws InterruptedException {
		QAFExtendedWebElement emp = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("brightspark.searchfield.loc"), employee));
		Validator.verifyTrue(emp.isDisplayed(), employee + "is missing", employee + "is present");
		emp.click();
		CommonUtils.pageload();
		Reporter.log(employee);
	}

	public void enterData(String str) {
		QAFExtendedWebElement text = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("textfield.loc"), str));
		text.sendKeys("dsfdffychysdfc");

	}

	public void selectCard(String card) {
		QAFExtendedWebElement selectcard = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("brightspark.selectcard.loc"), card));
		selectcard.click();
		CommonUtils.pageload();
	}

	public void brightSpark(String employee, String prize) {
		CommonUtils.pageload();
		for (int i = 0; i < getRequestnominee().size(); i++) {
			if (getRequestnominee().get(i).getText().contains(employee)) {
				Validator.verifyTrue(getRewardnames().get(i).getText().equalsIgnoreCase(prize), "reward for respective employee is not there", "reward for respective employee is not there");
				break;
			}
		}

	}

	@FindBy(locator = "table.row")
	private List<QAFWebElement> tableRow;
	@FindBy(locator = "table.cell")
	private List<QAFWebElement> tableCell;

	public void clickOnReward(String employee, String prize) {

		CommonUtils.pageload();
		for (int i = 0; i < getRequestnominee().size(); i++) {
			if (getRequestnominee().get(i).getText().contains(employee)) {
				getRewardnames().get(i).click();
				break;
			}
		}

	}

	public void veriyStatus(String prize,String employee, String status) {

		CommonUtils.pageload();
		for (int i = 0; i < getManagerStatus().size(); i++) {
			if (getRequestnominee().get(i).getText().contains(employee)&&getManagerStatus().get(i).getText().contains(status)) {
				getRewardnames().get(i).click();
				CommonUtils.pageload();
				break;
			}
		}

	}

	public void filllngInformation() {
		databean.fillRandomData();
		Validator.verifyTrue(getTextfield().isDisplayed(), "text fields are not present", "text fields are present");
		getTextfield().sendKeys(databean.getChallengingsituationsfaced());
		getTextfield().sendKeys(databean.getBenefits_accrued());
		getTextfield().sendKeys(databean.getSolution_provided());

		getTextfield().sendKeys(databean.getRational_for_nomination());
	}

	public void enterData() {
		databean.fillRandomData();
		getTextFieldForPatOnBack().sendKeys(databean.getContributionKey());
	}

	public void clickOnAward(String employee, String prize) {

		for (QAFWebElement nominee : getRequestnominee()) {
			for (QAFWebElement reward : getRewardnames()) {
				if (nominee.getText().equals(employee) && reward.getText().equals(prize)) {
					reward.click();
				}

			}
		}
	}
}
