package com.qmetry.qaf.example.Pages;

import java.util.List;

import javax.swing.JOptionPane;

import org.hamcrest.Matchers;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class LeavePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	HomePage homepage = new HomePage();

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

	@FindBy(locator = "Leave.title.loc")
	private QAFWebElement leavetitle;

	public QAFWebElement getLeaveTitle() {
		return leavetitle;
	}

	@FindBy(locator = "leaveDropdown.loc")
	private QAFWebElement leaveDropdown;

	public QAFWebElement getLeaveDRopdown() {
		return leaveDropdown;
	}

	@FindBy(locator = "leavetype.loc")
	private QAFWebElement leavetype;

	public QAFWebElement getLeaveType() {
		return leavetype;
	}

	@FindBy(locator = "menu.managerview")
	private QAFWebElement menuManagerView;
	@FindBy(locator = "leavestartdate.loc")
	private QAFWebElement startdate;
	@FindBy(locator = "leavendate.loc")
	private QAFWebElement leavendate;

	public QAFWebElement getStartdate() {
		return startdate;
	}

	public QAFWebElement getLeavendeate() {
		return leavendate;
	}

	@FindBy(locator = "available.leaves")
	private QAFWebElement availableLeaves;

	public QAFWebElement getAvailableLeaves() {
		return availableLeaves;
	}

	@FindBy(locator = "startdatecaledar.loc")
	private QAFWebElement startdatecaledar;
	@FindBy(locator = "enddatecaledar.loc")
	private QAFWebElement enddatecaledar;

	public QAFWebElement getStartdatecaledar() {
		return startdatecaledar;
	}

	public QAFWebElement getEnddatecaledar() {
		return enddatecaledar;
	}

	@FindBy(locator = "startdatecaledardates.dates")
	private List<QAFWebElement> startdatecaledardates;

	public List<QAFWebElement> getStartdatecaledardates() {
		return startdatecaledardates;
	}

	@FindBy(locator = "leaveReason.field")
	private QAFWebElement leaveReason;

	public QAFWebElement getLeaveReason() {
		return leaveReason;
	}

	@FindBy(locator = "leave.totalLeave")
	private QAFWebElement leaveTotalLeave;
	@FindBy(locator = "leave.leaveDuration")
	private QAFWebElement leaveDuration;

	public QAFWebElement getLeaveTotalLeave() {
		return leaveTotalLeave;
	}

	public QAFWebElement getLeaveDuration() {
		return leaveDuration;
	}

	@FindBy(locator = "leave.menulist")
	private List<QAFWebElement> leavemenulist;

	public List<QAFWebElement> getLeavemenulist() {
		return leavemenulist;
	}

	public void VerifyTitlePage(String title) {
		CommonUtils.pageload();
		QAFExtendedWebElement titlepage = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getPropertyValue("titlePage.loc"), title));
		if (titlepage.getText().equals(title)) {
			Reporter.logWithScreenShot("Title is verified");
		}
		CommonUtils.pageload();
		Validator.verifyThat(titlepage.getText(), Matchers.equalToIgnoringCase(title));
	}

	public void selectMenuFromMenuList(String menu) {
		CommonUtils.pageload();
		homepage.getHamburgerMenu().click();
		QAFTestBase.pause(2000);
		QAFExtendedWebElement menufrommenulist = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("R&R.loc"), menu));
		waitForPageToLoad();
		Validator.verifyTrue(menufrommenulist.isDisplayed(), menu + "menu is not present", menu + "menu is present");
		menufrommenulist.click();
		CommonUtils.pageload();
	}

	public void selectSubMenu(String submenu) {
		QAFTestBase.pause(2000);
		QAFExtendedWebElement submenuFromList = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("randrformanagerview.submenu"), submenu));
		Validator.verifyTrue(submenuFromList.isPresent(), submenu + "field is absent", submenu + "feld is present");
		submenuFromList.click();

	}

	public void clickOnLeavedropdown() {
		CommonUtils.pageload();
		getLeaveDRopdown().click();
		CommonUtils.pageload();
	}

	public void clickOnMenuforManagerView(String menu) {
		homepage.getHamburgerMenu().click();
		CommonUtils.pageload();
		QAFExtendedWebElement availableMenu = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getPropertyValue("menu.managerview"), menu));
		Validator.verifyTrue(availableMenu.isDisplayed(), menu + "menu is absent", menu + "menu is present");
		availableMenu.click();

	}

	public void selectLeaveType(String leavetype) {
		QAFExtendedWebElement leaveTypefromlist = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("leavetype.loc"), leavetype));
		leaveTypefromlist.click();
	}

	public void selectDate() {
		CommonUtils.pageload();
		Validator.verifyTrue(getStartdatecaledar().isPresent(), "start calendar icon is not present",
				"start calendar icon is present");
		getStartdatecaledar().click();
		CommonUtils.pageload();
		for (QAFWebElement alldates : getStartdatecaledardates())

			if (alldates.getText().equals("29")) {
				alldates.click();
				break;
			}
		CommonUtils.pageload();
		Validator.verifyTrue(getEnddatecaledar().isPresent(), "end calendar icon is not present",
				"start end icon is present");

		getEnddatecaledar().click();
		CommonUtils.pageload();
		for (QAFWebElement alldates : startdatecaledardates)

			if (alldates.getText().equals("29")) {
				alldates.click();
				break;
			}
		CommonUtils.pageload();
	}

	public void selectLeaveReason(String reason) {
		getLeaveReason().isPresent();
		getLeaveReason().click();
		QAFExtendedWebElement leavereason = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("leave.reasonfromlist"), reason));
		JOptionPane.showInputDialog("Before family click");
		Validator.verifyTrue(leavereason.isPresent(), reason + "is not there", reason + "is there");
		Actions act = new Actions(driver.getUnderLayingDriver());
		act.moveToElement(leavereason).click().perform();

		JOptionPane.showInputDialog("after family click");
		CommonUtils.pageload();
	}

	public void selectLeaveDay(String day) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		QAFExtendedWebElement leaveday = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("leave.day"), day));
		Validator.verifyTrue(leaveday.isDisplayed(), day + "day is not present", day + "day is present");
		leaveday.click();
	}

	public void appliedLeaveStatus() {
		if (getLeaveTotalLeave().getText().contains("1"))

		{
			Reporter.log("Leave successfully applied for a day");
		}
	}

	public void leaveHeaderMenuList() {

		String[] expected = { "Employee Name (ID)", "Applied Date", "Type", "Leave Duration ", "Leave Date ", "Status ",
				"Leave Reason ", "Project", "Manager's Comment ", "Actions", "Back Dated Leave" };

		if (expected.length != leavemenulist.size()) {
			Reporter.log("fail, wrong number of elements found");
		}

		for (int i = 0; i < expected.length; i++) {
			String optionValue = leavemenulist.get(i).getAttribute("innerHTML");

			if (optionValue.equals(expected[i])) {
				Reporter.log("passed on: " + optionValue);
			} else {
				Reporter.log("failed on: " + optionValue);
			}

		}

	}

	public void VisibleAppliedLeaveMessage(String message) {
		QAFExtendedWebElement leaveMessage = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("applied.leavemsg"), message));
		leaveMessage.isDisplayed();

	}

	public void checkLeaveBalance() {

		String days = "";
		// String day1 = "";
		String appliedLeaves = getLeaveTotalLeave().getText().toString().trim();
		Reporter.log("appled leaves:" + appliedLeaves + ":");
		Reporter.log("appliedLeaves.indexOf(\n):" + appliedLeaves.indexOf("\n"));
		days = appliedLeaves.substring(0, appliedLeaves.indexOf("\n"));

		int result = Integer.parseInt(days);

		Reporter.log("result" + result);
		int leaveBalance = 10;// its
		Reporter.log("available leavea" + getAvailableLeaves().getAttribute("innerHTML"));
		String availableLeaves = getAvailableLeaves().getText().toString().trim();
		int holidays = 5;
		Reporter.log("text of available leaves" + getAvailableLeaves().getText());
		Reporter.log("attribute of available leaves" + getAvailableLeaves().getAttribute("innerHTML"));
		if (leaveBalance == result - holidays) {
			Reporter.log("leaves applied successfully");
		}
		Reporter.log("leaves are not applied successfully");
	}

}
