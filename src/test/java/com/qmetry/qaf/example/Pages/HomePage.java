package com.qmetry.qaf.example.Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	TravelPage travel = new TravelPage();

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

	@FindBy(locator = "homepagemenufordeliverymanager.loc")
	private QAFWebElement homepagemenufordeliverymanager;

	public QAFWebElement getHomepagemenufordeliverymanager() {
		return homepagemenufordeliverymanager;
	}

	@FindBy(locator = "homePageTab.loc")
	private QAFWebElement homePageTab;

	@FindBy(locator = "logout.loc")
	private QAFWebElement logoutbtn;

	@FindBy(locator = "hamburgerMenu.loc")
	private QAFWebElement hamburgerMenu;

	public QAFWebElement getLogoutbtn() {
		return logoutbtn;
	}

	@FindBy(locator = "switchView.loc")
	private QAFWebElement switchViewButton;

	@FindBy(locator = "R&R.loc")
	private QAFWebElement RandR;

	@FindBy(locator = "list.menu.loc")
	private List<Component> menuList;

	public List<Component> getMenuList() {
		return menuList;
	}

	@FindBy(locator = "hamburge.submenulist.loc")
	private List<Component> submenulist;

	public List<Component> getSubmenulist() {
		return submenulist;
	}

	@FindBy(locator = "nominate.loc")
	private QAFWebElement nominate;

	@FindBy(locator = "bright_spark.loc")
	private QAFWebElement brightspark;

	@FindBy(locator = "scrolldown.loc")
	private QAFWebElement scrolldown;

	@FindBy(locator = "homepagemenuformanageview.loc")
	private QAFWebElement managerview;

	public QAFWebElement getManagerview() {
		return managerview;
	}

	public QAFWebElement getScrolldown() {
		return scrolldown;
	}

	public QAFWebElement getSwitchViewButton() {
		return switchViewButton;
	}

	public QAFWebElement getHomePageTab() {
		return homePageTab;
	}

	public QAFWebElement getHamburgerMenu() {
		return hamburgerMenu;
	}

	public QAFWebElement getRandR() {
		return RandR;
	}

	public QAFWebElement getNominate() {
		return nominate;
	}

	public QAFWebElement getBrightspark() {
		return brightspark;
	}

	public void clickOnTab(String request) throws InterruptedException {
		QAFExtendedWebElement msg = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("homePageTab.loc"), request));
		CommonUtils.pageload();
		Validator.verifyTrue(msg.isDisplayed(), "fields are not displaying", "Fields are displaying");
		msg.click();

	}

	public void clickOnLogOut() throws InterruptedException {
		QAFTestBase.pause(3000);// use as was getting loading issue
		Validator.verifyTrue(getLogoutbtn().isDisplayed(), "LogOut button is missing", "logout button is present");
		getLogoutbtn().click();
		CommonUtils.pageload();
		QAFTestBase.pause(3000);

	}

	public void openMainMenu(String menu) throws InterruptedException {
		QAFTestBase.pause(2000);
		CommonUtils.pageload();
		getHamburgerMenu().click();
		QAFTestBase.pause(2000);
		QAFExtendedWebElement sub = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getPropertyValue("test1.loc"), menu));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", sub);
		Validator.verifyTrue(sub.isDisplayed(), "menu is not displaying", "menu is displaying");
		sub.click();

	}

	public void openMainMenuForManagerView(String menu) {
		CommonUtils.pageload();
		getHamburgerMenu().click();
		QAFTestBase.pause(2000);
		QAFExtendedWebElement sub = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("homepagemenuformanageview.loc"), menu));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", sub);
		QAFTestBase.pause(2000);
		CommonUtils.pageload();
		sub.click();
	}

	public void openMainMenuForDeliveryManagerView(String menu) throws InterruptedException {
		CommonUtils.pageload();
		getHamburgerMenu().click();
		QAFTestBase.pause(2000);
		QAFExtendedWebElement sub = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("homepagemenuformanageview.loc"), menu));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", sub);
		QAFTestBase.pause(2000);
		CommonUtils.pageload();
		sub.click();
	}

	public void clivkOnRandR() {
		getRandR().click();
	}

	public void naminateMenu(String submenu) throws InterruptedException {
		CommonUtils.pageload();
		QAFTestBase.pause(2000);

		QAFExtendedWebElement sub = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getPropertyValue("nominate.loc"), submenu));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", sub);
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		Validator.verifyTrue(sub.isDisplayed(), "menu is not displayed", "menu is displayed");
		sub.click();
		CommonUtils.pageload();

	}

	public void submenuOfRandR(String submenu) throws InterruptedException {
		QAFExtendedWebElement hiddenMenu = new QAFExtendedWebElement(

				String.format(ConfigurationManager.getBundle().getPropertyValue("randrformanagerview.submenu"),
						submenu));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", hiddenMenu);
		CommonUtils.pageload();
		Validator.verifyTrue(hiddenMenu.isDisplayed(), "menu is not displaying", "menu is displaying");
		hiddenMenu.click();
	}

	public void clickOnBright(String bright) throws InterruptedException {
		QAFExtendedWebElement sub = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("bright_spark.loc"), bright));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", sub);
		CommonUtils.pageload();
		sub.click();
		CommonUtils.pageload();

	}

	public void testComponet() {
		getHamburgerMenu().click();

	}

	public void logout() {
		boolean str = getLogoutbtn().isDisplayed();
		Validator.verifyTrue(str, "LogOut button is not there", "LogOut button is there");

	}

	public void displaymenuformagarView(String menu) {
		getHamburgerMenu().click();
		CommonUtils.pageload();
		QAFExtendedWebElement sub = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getPropertyValue("menu.managerview"), menu));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", sub);
		CommonUtils.pageload();
		Validator.verifyTrue(sub.isDisplayed(), "menu is not there", "menu is there");
		sub.click();

	}

	public void submenuForMyView(String submenu) {
		CommonUtils.pageload();
		QAFExtendedWebElement sub = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getPropertyValue("submenu.myview"), submenu));
		CommonUtils.pageload();
		Validator.verifyTrue(sub.isDisplayed(), "field is present", "field is not present");
		sub.click();
	}
	public void clickOnLeaveMenu(String menu)
	{
		
		QAFExtendedWebElement hiddenMenu = new QAFExtendedWebElement(

				String.format(ConfigurationManager.getBundle().getPropertyValue("randrformanagerview.submenu"),
						menu));
		CommonUtils.pageload();
		Validator.verifyTrue(hiddenMenu.isDisplayed(), "menu are not displaying", "menu are displaying");
		hiddenMenu.click();
	}
}
